const express = require("express");
const { verify } = require("../middleware/auth");
const {
  createProduct,
  getProducts,
  getActiveProducts,
  getProduct,
  updateProduct,
  archiveProduct,
} = require("../controllers/productController");

const router = express.Router();

router.get("/active", getActiveProducts);

router.get("/:id", getProduct);

router.use(verify);

router.post("/create", createProduct);

router.get("/", getProducts);


router.put("/update/:id", updateProduct);

router.patch("/archive/:id", archiveProduct);

module.exports = router;
