const express = require("express");
const {
  createOrder,
  getOrders,
  getMyOrders,
  removeOrder,
  checkoutPay,
  addMyOrderQuantity,
  subtractMyOrderQuantity,
  getUserOrdersHistory,
} = require("../controllers/orderController");
const {
  registerUser,
  loginUser,
  getUserDetails,
  setAsAdmin,
  updateUserDetails,
  userPasswordVerify,
  userPasswordUpdate,
} = require("../controllers/userController");
const { verify } = require("../middleware/auth");

const router = express.Router();

// User Routes
router.post("/register", registerUser);

router.post("/login", loginUser);

router.put("/:id/setAsAdmin", verify, setAsAdmin);

router.get("/userDetails", verify, getUserDetails);

router.put("/userUpdateDetails", verify, updateUserDetails);

router.post("/userPasswordVerify", verify, userPasswordVerify);

router.patch("/userPasswordUpdate", verify, userPasswordUpdate);

// Order Routes
router.use(verify);

router.post("/checkout", createOrder);

router.post("/checkout/pay", checkoutPay);

router.get("/ordersHistory", getUserOrdersHistory);

router.get("/orders", getOrders);

router.delete("/removeOrder", removeOrder);

router.get("/myOrders", getMyOrders);

router.patch("/myOrders/addQuantity/:productId/:quantity", addMyOrderQuantity);

router.patch(
  "/myOrders/subQuantity/:productId/:quantity",
  subtractMyOrderQuantity
);

module.exports = router;
