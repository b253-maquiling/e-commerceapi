const mongoose = require("mongoose");

const orderHistorySchema = new mongoose.Schema(
  {
    userId: {
      type: String,
      required: [true, "User id is requried"],
      required: [true, "User id is requried"],
    },
    products: [
      {
        productId: {
          type: String,
          required: [true, "Product id is required"],
        },

        quantity: {
          type: Number,
          required: [true, "Product quantatiy is required"],
        },

        subTotal: {
          type: Number,
          required: [true, "Sub Total is required"],
        },
      },
    ],
    totalAmount: {
      type: Number,
      required: [true, "Total price is required"],
    },
    purchasedOn: {
      type: Date,
      default: new Date(),
    },
    paymentStatus: {
      type: String,
      default: "Paid",
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("OrderHistory", orderHistorySchema);
