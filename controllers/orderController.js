const asyncHandler = require("express-async-handler");
const mongoose = require("mongoose");
const auth = require("../middleware/auth");

const Product = require("../models/productModel");
const Order = require("../models/orderModel");
const OrderHistory = require("../models/orderHistoryModel");

// @desc Create an order
// @route POST /user/checkout
// @access private
const createOrder = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const { productId, quantity } = req.body;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).send("Invalid product ID");
  }

  const product = await Product.findById(productId);

  if (!product) {
    return res.status(404).send("Product not found!");
  }

  if (!product.isActive) {
    return res.status(400).json({
      errMessage: "Product is not available!",
      productName: product.productName,
      isActve: product.isActive,
    });
  }

  if (userDataToken.user.id) {
    // check if user has an existing order
    const existingOrder = await Order.findOne({
      userId: userDataToken.user.id,
    });
    if (existingOrder) {
      // check if product is already in the order
      const existingProduct = existingOrder.products.find(
        (product) => product.productId === productId
      );
      if (existingProduct) {
        if (existingProduct.quantity + quantity <= 0) {
          // remove product from order
          existingOrder.products = existingOrder.products.filter(
            (product) => product.productId !== productId
          );
        } else {
          // else update existing quantity and subtotal
          existingProduct.quantity += quantity;
          existingProduct.subTotal += product.price * quantity;
        }
        // update the total amount base on the new request
        existingOrder.totalAmount = existingOrder.products.reduce(
          (total, product) => total + product.subTotal,
          0
        );
        await existingOrder.save();
        return res.status(201).json(existingOrder);
      } else {
        // else product is not in the order push the request
        existingOrder.products.push({
          productId: product.id,
          quantity,
          subTotal: product.price * quantity,
        });
        existingOrder.totalAmount += product.price * quantity;
        await existingOrder.save();
        return res.status(201).json(existingOrder);
      }
    } else {
      // if user dont have existing order
      const order = await Order.create({
        userId: userDataToken.user.id,
        products: [
          {
            productId: product.id,
            quantity,
            subTotal: product.price * quantity,
          },
        ],
        totalAmount: product.price * quantity,
      });
      return res.status(201).json(order);
    }
  } else {
    return res.status(401).json({ auth: false });
  }
});

// @desc Get all user's order
// @route GET /user/orders
// @access private
const getOrders = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  if (userDataToken.user.isAdmin) {
    const orders = await Order.find({});
    if (orders.length <= 0) {
      return res.status(404).send("No orders are made");
    }
    return res.status(200).json(orders);
  }

  return res.status(403).send("Access Forbidden");
});

// @desc Get user orders
// @route GET /user/myOrders
// @access private
const getMyOrders = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  if (userDataToken.user.id) {
    const myOrders = await Order.find({ userId: userDataToken.user.id });
    return res.status(200).json(myOrders);
  }
  return res.status(401).json({ auth: false });
});

// @desc Add quantity Order
// @route PATCH /user/myOrders/addQuantity/:productId/:quantity
// @access private
const getProductPrice = async (productId) => {
  const product = await Product.findById(productId);
  return product.price;
};

const addMyOrderQuantity = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const { productId, quantity } = req.params;

  const myOrders = await Order.find({ userId: userDataToken.user.id });

  await Promise.all(
    myOrders.map(async (order) => {
      const productIndex = order.products.findIndex(
        (p) => p.productId === productId
      );
      if (productIndex !== -1) {
        const productPrice = await getProductPrice(productId);
        order.products[productIndex].quantity += parseInt(quantity, 10);
        order.products[productIndex].subTotal =
          order.products[productIndex].quantity * productPrice;
        order.totalAmount = order.products.reduce(
          (acc, p) => acc + p.subTotal,
          0
        );
        await order.save();
        return res.status(200).json(order);
      }
    })
  );
});

const subtractMyOrderQuantity = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const { productId, quantity } = req.params;

  const myOrders = await Order.find({ userId: userDataToken.user.id });

  await Promise.all(
    myOrders.map(async (order) => {
      const productIndex = order.products.findIndex(
        (p) => p.productId === productId
      );
      if (productIndex !== -1) {
        const productPrice = await getProductPrice(productId);
        const currentQuantity = order.products[productIndex].quantity;
        const newQuantity = currentQuantity - parseInt(quantity, 10);
        if (newQuantity <= 0) {
          // remove the product from the order if the new quantity is less than or equal to zero
          order.products.splice(productIndex, 1);
        } else {
          order.products[productIndex].quantity = newQuantity;
          order.products[productIndex].subTotal = newQuantity * productPrice;
        }
        order.totalAmount = order.products.reduce(
          (acc, p) => acc + p.subTotal,
          0
        );
        await order.save();
        return res.status(200).json(order);
      }
    })
  );
});

// @desc Remove user order/s
// @route DELETE /user/removeOrders
// @access private
const removeOrder = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const { productId } = req.body;

  if (!mongoose.Types.ObjectId.isValid(productId)) {
    return res.status(400).send("Invalid product ID");
  }

  if (userDataToken.user.id) {
    const existingOrder = await Order.findOne({
      userId: userDataToken.user.id,
    });

    if (existingOrder) {
      const existingProduct = existingOrder.products.find(
        (product) => product.productId === productId
      );

      if (existingProduct) {
        await Order.updateOne(
          { userId: userDataToken.user.id },
          { $pull: { products: { productId: productId } } }
        );
        return res.status(201).send(`${productId} is removed from the cart!`);
      } else {
        return res.status(404).send(`${productId} is not in the cart!`);
      }
    } else {
      return res
        .status(404)
        .send(`${userDataToken.user.ud} doesn't have any order yet!`);
    }
  } else {
    return res.status(401).json({ auth: false });
  }
});

const checkoutPay = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);
  const userOrder = await Order.findOne({
    userId: userDataToken.user.id,
  });

  if (userDataToken.user.id) {
    const orderHistory = await OrderHistory.create({
      userId: userDataToken.user.id,
      products: userOrder.products,
      totalAmount: userOrder.totalAmount,
    });
    await Order.deleteMany({ userId: userDataToken.user.id });
    return res.status(201).json({ success: "Orders are paid", orderHistory });
  } else {
    return res.status(401).json({ auth: false });
  }
});

const getUserOrdersHistory = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);
  const userOrderHistory = await OrderHistory.find({
    userId: userDataToken.user.id,
  });

  if (!userOrderHistory) {
    return res.status(404).send(false);
  }

  const allProducts = userOrderHistory.flatMap((order) =>
    order.products.map((product) => ({
      ...product.toObject(),
      purchasedOn: order.purchasedOn,
    }))
  );

  return res.status(200).json(allProducts);
});

module.exports = {
  createOrder,
  getOrders,
  getMyOrders,
  removeOrder,
  checkoutPay,
  addMyOrderQuantity,
  subtractMyOrderQuantity,
  getUserOrdersHistory,
};
