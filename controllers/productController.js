const asyncHandler = require("express-async-handler");
const { default: mongoose } = require("mongoose");
const auth = require("../middleware/auth");

const Product = require("../models/productModel");

// @desc Create a product
// @route POST /product/create
// @access private
const createProduct = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const { productName, description, price } = req.body;

  const duplicateProduct = await Product.findOne({
    productName: req.body.productName,
  });

  if (!productName || !description || !price) {
    return res.status(400).send("All fields are mandatory!");
  }

  if (userDataToken.user.isAdmin) {
    if (!duplicateProduct?.productName) {
      const product = await Product.create({
        productName,
        description,
        price,
      });

      return res.status(201).json(product);
    }
    return res.status(400).send("Duplicate Product!");
  } else {
    return res.status(401).json({ auth: false });
  }
});

// @desc Get all products
// @route GET /product/all
// @access private
const getProducts = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  if (!userDataToken.user.isAdmin) {
    return res.status(401).json({ auth: false });
  }

  const products = await Product.find().sort({ updatedOn: -1, createOn: -1 });
  return res.status(200).json(products);
});

// @desc Get active products
// @route GET /product/active
// @access public
const getActiveProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({ isActive: true }).sort({updatedOn: -1, createOn: -1});

  if (products.length <= 0) {
    return res.status(404).send("No Products Available");
  }
  return res.status(200).json(products);
});

// @desc Get product
// @route GET /product/:id
// @access public
const getProduct = asyncHandler(async (req, res) => {
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).send("Invalid product ID");
  }
  const product = await Product.findById(req.params.id);

  if (!product) {
    return res.status(404).send("Product not found!");
  }

  return res.status(200).json(product);
});

// @desc Update product
// @route PUT /product/update/:id
// @access private
const updateProduct = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).send("Invalid product ID");
  }

  const product = await Product.findById(req.params.id);

  if (!product) {
    return res.status(404).send("Product not found!");
  }

  if (userDataToken.user.isAdmin) {
    const updateObj = {
      ...req.body,
      updatedOn: new Date(),
    };

    const updateProduct = await Product.findByIdAndUpdate(
      req.params.id,
      updateObj,
      { new: true }
    );

    res.status(200).json(updateProduct);
  } else {
    return res.status(401).json({ auth: false });
  }
});

// @desc Archive product
// @route PATCH /product/archive/:id
// @access private
const archiveProduct = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).send("Invalid product ID");
  }

  const product = await Product.findById(req.params.id);

  if (!product) {
    return res.status(404).send("Product not found!");
  }

  if (userDataToken.user.isAdmin) {
    const archiveObj = {
      ...req.body,
      updatedOn: new Date(),
    };

    const archiveProduct = await Product.findByIdAndUpdate(
      req.params.id,
      archiveObj,
      { new: true }
    );

    res.status(200).json(archiveProduct);
  } else {
    return res.status(401).json({ auth: false });
  }
});

module.exports = {
  createProduct,
  getProducts,
  getActiveProducts,
  getProduct,
  updateProduct,
  archiveProduct,
};
