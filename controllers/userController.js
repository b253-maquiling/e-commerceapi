const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const auth = require("../middleware/auth");

// @desc Register a user
// @route POST /user/register
// @access public
const registerUser = asyncHandler(async (req, res) => {
  const { firstName, lastName, email, password, mobileNum } = req.body;

  if (!firstName || !lastName || !email || !password || !mobileNum) {
    res.status(400).send(`All fields are mandatory!`);
  }
  const userAvailable = await User.findOne({ email });
  if (userAvailable) {
    return res.status(400).send(false);
  }

  const hashedPassword = await bcrypt.hash(password, 10);

  const user = await User.create({
    firstName,
    lastName,
    email,
    mobileNum,
    password: hashedPassword,
  });

  if (user) {
    return res.status(201).send(true);
  } else {
    return res.status(400).json("User data is not valid");
  }
});

// @desc Login user
// @route POST /user/login
// @access private
const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(400).send("All fields are mandatory!");
  }

  const user = await User.findOne({ email });

  if (user && (await bcrypt.compare(password, user.password))) {
    const accessToken = jwt.sign(
      {
        user: {
          id: user._id,
          email: user.email,
          isAdmin: user.isAdmin,
        },
      },
      process.env.ACCESS_TOKEN_SECRET,
      {}
    );
    return res.status(200).json({
      email: user.email,
      isAdmin: user.isAdmin,
      token: accessToken,
    });
  } else {
    return res.status(401).send(false);
  }
});

// @desc User details
// @route GET /user/:id/userDetails
// @access private
const getUserDetails = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const userDetails = await User.findById(userDataToken.user.id);
  if (userDataToken.user.id !== userDetails.id) {
    // Can view user details because admin access
    /* if (userDataToken.user.isAdmin) {
      return res.status(200).send(userDetails);
    } */
    return res
      .status(403)
      .send(
        `${userDataToken.user.id} is forbidden to access ${userDetails.id} Details`
      );
  }
  if (!userDetails) {
    return res.status(404).send(`${userDataToken.user.id} is not found!`);
  } else {
    userDetails.password = "";
    return res.status(200).json(userDetails);
  }
});

// @desc Change/Set user to admin
// @route PUT /user/:id/setAsAdmin
// @access private
const setAsAdmin = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const userDetails = await User.findById(req.params.id);

  if (!userDetails) {
    return res.status(404).send("No user found!");
  }

  if (userDataToken.user.isAdmin) {
    const setUserAdmin = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    res.status(200).json(setUserAdmin);
  } else {
    return res.status(401).json({ auth: false });
  }
});

// @desc Update user details
// @route PUT /user/userUpdateDetails
// @access private
const updateUserDetails = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const userDetails = await User.findById(userDataToken.user.id);

  if (!userDetails) {
    return res.status(404).send("No user found!");
  }

  if (userDataToken.user) {
    const updateUser = await User.findByIdAndUpdate(
      userDataToken.user.id,
      req.body,
      {
        new: true,
      }
    );

    res.status(200).json(updateUser);
  } else {
    return res.status(401).json({ auth: false });
  }
});

// @desc Verify user password
// @route PUT /user/userPasswordVerify
// @access private
const userPasswordVerify = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const user = await User.findOne({ email: userDataToken.user.email });

  if (user && (await bcrypt.compare(req.body.password, user.password))) {
    return res.status(200).send(true);
  } else {
    return res.status(401).send(false);
  }
});

// @desc Update user password
// @route PATCH /user/userPasswordUpdate
// @access private
const userPasswordUpdate = asyncHandler(async (req, res) => {
  const userDataToken = auth.decode(req.headers.authorization);

  const user = await User.findOne({ email: userDataToken.user.email });

  const hashedPassword = await bcrypt.hash(req.body.password, 10);

  if (user) {
    const updatePassword = await User.findByIdAndUpdate(
      user._id,
      { password: hashedPassword },
      {
        new: true,
      }
    );

    res.status(200).json(updatePassword);
  } else {
    return res.status(400).send(false);
  }
});

module.exports = {
  registerUser,
  loginUser,
  getUserDetails,
  setAsAdmin,
  updateUserDetails,
  userPasswordVerify,
  userPasswordUpdate
};
